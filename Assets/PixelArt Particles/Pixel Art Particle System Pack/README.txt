-----------------------------------------------------------------
PIXEL ART PARTICLE SYSTEM PACK by Jesus Gonzalez
-----------------------------------------------------------------

Thank you very much for buying Pixel Art Particle Pack.

This package contains with 44 pixel art style particle systems (Shuriken).
You can customize them and the provided textures if you need new variations.
Almost every included particle system is suitable to be used in both 2D and 3D scenes.

Usage:
- Drag the prefabs into your scene or use code to instantiate them.
- For the "burst" type particles use the Emit() and Play() methods in your code.

Contents:
- Fire: 5 effects.
- Explosions: 6 effects.
- Smoke: 16 effects.
- Water: 4 effects.
- Electricity: 9 effects.
- Light: 4 effects.

A demo scene is also included.

More effects will be added in future updates! Free of charge if you already bought this.
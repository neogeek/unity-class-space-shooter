﻿using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public GameObject laserObject;
	public GameObject missileObject;
	public Transform[] fireLaserPositions;
	public HUDController hud;

	public GameObject explosionObject;

	private float thrust = 40.0f;

	private Rigidbody2D rb;

	private WaitForSeconds delay = new WaitForSeconds(0.08f);

	private bool isFiring = false;

	private float health = 10.0f;
	private float maxHealth = 10.0f;

	private LevelController levelController;

	private SuperLaserController superLaserController;

	void Awake () {

		rb = gameObject.GetComponent<Rigidbody2D>();

		levelController = GameObject.Find("LevelController").GetComponent<LevelController>();
		superLaserController = gameObject.transform.Find("FireSuperLaser").gameObject.GetComponent<SuperLaserController>();

	}

	void Update () {

		Vector3 screenPos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));

		gameObject.transform.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(screenPos.y - gameObject.transform.position.y, screenPos.x - transform.position.x) * Mathf.Rad2Deg);

		if (Input.GetKey("space")) {

			rb.AddForce(transform.right * thrust);

		}

		if (Input.GetMouseButton(0)) {

			StartCoroutine(FireLaser());

		}

		if (Input.GetMouseButtonDown(1)) {

			superLaserController.On();

		} else if (Input.GetMouseButtonUp(1)) {

			superLaserController.Off();

		}

		if (Input.GetKeyDown("e")) {

			Instantiate(missileObject, gameObject.transform.position, gameObject.transform.rotation * Quaternion.Euler(0, 0, -90.0f));

		}

		if (health <= 0) {

			Die();

		}

	}

	public void Hit (float damage) {

		health = health - damage;

		hud.SetLifeBar(health / maxHealth);

	}

	void Die () {

		Instantiate(explosionObject, gameObject.transform.position, Quaternion.identity);

		levelController.Reset();

		Destroy(gameObject);

	}

	IEnumerator FireLaser () {

		if (!isFiring) {

			isFiring = true;

			foreach (Transform fireLaserPosition in fireLaserPositions) {

				Instantiate(laserObject, fireLaserPosition.position - new Vector3(0, 0, -1), gameObject.transform.rotation);

			}

			yield return delay;

			isFiring = false;

		}

	}

}

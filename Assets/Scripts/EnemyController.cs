﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

	public GameObject laserObject;
	public Transform[] fireLaserPositions;

	public GameObject explosionObject;

	private GameObject player;

	private bool isFiring = false;

	private float rateOfFire = 1.0f;

	private float health = 10.0f;

	private float playerTrackingSpeed = 1.0f;

	void Awake () {

		player = GameObject.Find("Player");

	}

	void Update () {

		if (player) {

			gameObject.transform.right = player.transform.position - gameObject.transform.position;

			gameObject.transform.position = Vector2.Lerp(gameObject.transform.position, player.transform.position, playerTrackingSpeed * Time.deltaTime);

			if (!isFiring) {

				StartCoroutine(FireLaser());

			}

		}

		if (health <= 0) {

			Die();

		}

	}

	public void Hit (float damage) {

		health = health - damage;

	}

	void Die () {

		Instantiate(explosionObject, gameObject.transform.position, Quaternion.identity);

		Destroy(gameObject);

	}

	IEnumerator FireLaser () {

		isFiring = true;

		yield return new WaitForSeconds(Random.Range(0.2f, 2.0f) * rateOfFire);

		foreach (Transform fireLaserPosition in fireLaserPositions) {

			Instantiate(laserObject, fireLaserPosition.position - new Vector3(0, 0, -1), gameObject.transform.rotation);

		}

		isFiring = false;

	}

}

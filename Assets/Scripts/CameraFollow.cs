﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

	private Transform objectTransformToFollow;

	private Camera mainCamera;

	private float dampRate = 0.3f;
	private Vector3 velocity = Vector3.zero;

	void Awake () {

		mainCamera = Camera.main;

		objectTransformToFollow = GameObject.Find("Player").transform;

	}

	void Update () {

		if (objectTransformToFollow) {

			// Vector3 point = mainCamera.WorldToViewportPoint(objectTransformToFollow.position);
			// Vector3 delta = objectTransformToFollow.position - mainCamera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z));

			// Vector3 destination = mainCamera.transform.position + delta;

			// mainCamera.transform.position = Vector3.SmoothDamp(mainCamera.transform.position, destination, ref velocity, dampRate);

			mainCamera.transform.position = Vector3.SmoothDamp(
				mainCamera.transform.position, new Vector3(
					objectTransformToFollow.transform.position.x,
					objectTransformToFollow.transform.position.y,
					mainCamera.transform.position.z
				),
				ref velocity,
				dampRate
			);

		}

	}

}

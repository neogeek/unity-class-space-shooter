﻿using UnityEngine;

public class EnemyLaserController : MonoBehaviour {

	public GameObject explosionObject;

	private float speed = 800.0f;

	private Rigidbody2D rb;

	void Awake () {

		rb = gameObject.GetComponent<Rigidbody2D>();

	}

	void Start () {

		rb.AddRelativeForce(new Vector3(1, 0, 0) * speed);

	}

	void OnCollisionEnter2D (Collision2D other) {

		if (other.gameObject.name.Contains("Player")) {

			other.gameObject.GetComponent<PlayerController>().Hit(1.0f);

		}

		Instantiate(explosionObject, gameObject.transform.position, Quaternion.identity);

		Destroy(gameObject);

	}

}

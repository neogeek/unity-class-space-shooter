﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDController : MonoBehaviour {

	public Texture[] lifeBarImages;

	public GUITexture lifeBar;

	public void SetLifeBar (float health) {

		lifeBar.texture = lifeBarImages[(int)Mathf.Lerp(0, lifeBarImages.Length, health)];

	}

}

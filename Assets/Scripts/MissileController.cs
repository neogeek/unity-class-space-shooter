﻿using UnityEngine;

public class MissileController : MonoBehaviour {

	public GameObject explosion;
	public LayerMask laserLayerHitMask;

	private float explosionRadius = 2.0f;
	private float explosionDamage = Mathf.Infinity;

	void OnCollisionEnter2D (Collision2D other) {

		Collider2D[] hitColliders = Physics2D.OverlapCircleAll(gameObject.transform.position, explosionRadius, laserLayerHitMask);

		foreach (Collider2D collider in hitColliders) {

			collider.gameObject.GetComponent<EnemyController>().Hit(explosionDamage);

		}

		Instantiate(explosion, gameObject.transform.position, Quaternion.identity);

		Destroy(gameObject);

	}

}

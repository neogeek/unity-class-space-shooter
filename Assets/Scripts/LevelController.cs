﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour {

	public void Reset () {

		StartCoroutine("ResetLevel");

	}

	IEnumerator ResetLevel () {

		yield return new WaitForSeconds(2);

		SceneManager.LoadScene(0);

	}

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperLaserController : MonoBehaviour {

	public GameObject missParticleEmitter;
	public LayerMask laserLayerHitMask;

	private LineRenderer lineRenderer;

	private bool laserActive = false;

	void Awake () {

		lineRenderer = gameObject.GetComponent<LineRenderer>();

	}

	void FixedUpdate () {

		if (laserActive) {

			RaycastHit2D hit = Physics2D.Raycast(gameObject.transform.position, gameObject.transform.right, Mathf.Infinity, laserLayerHitMask);

			lineRenderer.positionCount = 2;

			lineRenderer.SetPosition(0, gameObject.transform.position);
			lineRenderer.SetPosition(1, hit.point);

			Debug.DrawRay(gameObject.transform.position, gameObject.transform.right, Color.green);

			if (hit.collider.tag.Contains("Enemy")) {

				hit.collider.gameObject.GetComponent<EnemyController>().Hit(Mathf.Infinity);

			} else {

				Instantiate(missParticleEmitter, hit.point, Quaternion.identity);

			}

		} else {

			lineRenderer.positionCount = 0;

		}

	}

	public void On () {

		if (!laserActive) {

			laserActive = true;

			lineRenderer.enabled = true;

		}

	}

	public void Off () {

		if (laserActive) {

			laserActive = false;

			lineRenderer.enabled = false;

		}

	}

}
